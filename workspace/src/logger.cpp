#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc,char* argv[])
{
	int buf[3];
	mkfifo("/tmp/tenis_fifo", 0666);

	int fd = open("/tmp/tenis_fifo", O_RDONLY);
	if(fd < 0){
		perror("Error abriendo tuberia");
	}

	int leidos = read(fd,buf, 3*sizeof(int));
	while(leidos > 0){
		if (leidos>=3*sizeof(int))
			printf("Punto del jugador %d. %d-%d\n",buf[0],buf[1],buf[2]);
		leidos = read(fd,buf, 3*sizeof(int));
	}
	
	close(fd);
	unlink("/tmp/tenis_fifo");
	
	return 0;   
}
