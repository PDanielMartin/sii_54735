# Changelog
Registro de los cambios realizados en el laboratorio de SII por Pablo Daniel Martín de Domingo


## [1.3.0] - 2021-11-22
### Added
- Añadido programa de IA bot.
- Añadido logger.
- El juego termina al alcanzar 3 puntos.

## [1.2.0] - 2021-10-26
### Added
- Añadido movimiento a esferas y raquetas.
- Añadido readme
- La esfera reduce su radio con el tiempo. 

## [1.1.0] - 2021-10-25
### Added
- Añadido Changelog.md

### Changed
- Añadido autoría en Esferas.cpp
